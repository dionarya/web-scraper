require('colors')
const express = require('express');
const fs      = require('fs');
const app     = express();
const port    = process.env.PORT || 3000;
const request = require('request');
const cheerio = require('cheerio');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
 let url = "https://jadwalsholat.org/adzan/monthly.php?id=307";
 request.get(url, (err, response, html) => {
    if(!err) {
      let $ = cheerio.load(html);

      $('table.table_adzan tr[align=center]').each((i, value) => {
        $(value).find('td').each((j, data) => {
            if ($(value).attr('class') === 'table_highlight')
                return process.stdout.write($(data).text().red + '\t');
            return process.stdout.write($(data).text() + '\t');
         });

         process.stdout.write('\n');
      });
    }
  });
});

app.get('/artikel/:id', (req, res) => {
  let url;
  let urlData = [
      "https://www.sekolahkoding.com/blog/jangan-sembunyi",
      "https://www.sekolahkoding.com/blog/afrian-junir-dari-admin-game-online-ke-web-developer"
    ];

(req.params.id===0) ? url=urlData[0] : url=urlData[1];
  request.get(url, (err, response, html) => {
      if(!err){
        let $ = cheerio.load(html);
        $('div.is-article').each(function(i, artikel){
         let artikelnya = $(artikel).text();
          res.send(artikelnya);
        });
      }
  });


});

app.listen(port, () => {console.log(`MAGIC run on 127.0.0.1:${port}`)});